# Pacman Configuration

Pacman configuration (fizzy-compliant).

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-pacman/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
